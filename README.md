# PC Setup Script

This project holds a script that sets up a computer with the following apps:

- Google Chrome
- Git
- Visual STudio Code
- Notepad++
- Office 365 Business
- Nodejs
- 7Zip
- VLC
- Postman

And sets up git configurations.
Provided by [chocolatey](https://chocolatey.org/), just run chocolately.bat in administrator mode.
